//Soal 1 Looping While
console.log("Soal No.1 Looping Menggunakan While")
var indeks = 2;
var limit1 = 20;
var limit2 = 0;
var increment = 2;

console.log('LOOPING PERTAMA');

// KONDISI WHILE 1
while (indeks <= limit1) {
  console.log(indeks + ' - I Love Coding')
  indeks += increment;
}

console.log('LOOPING KEDUA')
indeks = indeks - 2;

// KONDISI WHILE 2
while (indeks > limit2) {
  console.log(indeks + ' - I will become a mobile developer')
  indeks -= increment;
}
console.log("====================================================\n")

//Soal 2 Looping Menggunakan For
console.log("Soal No.2 Looping Menggunakan For")
var num =1;

for(i=num; i<=20; i++){
    if((i%3)===0 && (i%2)===1){
        console.log(i + '- I Love Coding')
    }else if((i%2)===0){
        console.log(i + '- Berkualitas')
    }else
    console.log(i + '- Santai')
}
console.log("====================================================\n")

//Soal 3 Membuat Persegi Panjang #
console.log("Soal No.3 Membuat Persegi Panjang #")
var persegi = "####";
for(const char of persegi){
    console.log(char.repeat(8));
}
console.log("====================================================\n")

//Soal 4 Membuat Tangga #
console.log("Soal No.4 Membuat Tangga #")
var a = 7;
var b='';

for(var k=0; k<a; k++){
    for(var l=0; l<=k; l++){
        b +='#'
    }b += '\n'
}console.log(b)
console.log("====================================================\n")

//Soal 5 Membuat Papan Catur
console.log("Soal No.5 Membuat Papan Catur")
for(i=1; i<=8; i++){
    if((i%2)===1){
        console.log(' # # # #')
    }else
    console.log('# # # #')
}
console.log("====================================================\n")