// Soal 1 Animal Class
console.log("Soal 1 Animal Class")
class Animal {
    constructor(name){
        this._name = name;
        this.legs = 4;
        this.cold_blooded = false;
    }
    get name(){
        return this._name;
    }
    set name(x){
        return this._name =x;
    }
}
class Ape extends Animal {
    constructor(name, cold_blooded, legs){
        super(cold_blooded);
        this.name = name;
        this.legs =2;
    }
    yell(){
        console.log("Auoooo");
    }
} 
class Frog extends Animal {
    constructor(name, cold_blooded, legs){
        super(cold_blooded);
        this.name = name;
    }
    jump(){
        console.log("Hop Hop");
    }
}
var sheep = new Animal("shaun",'4','false');
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false
console.log("===================================\n")
var sungokong = new Ape("kera sakti","false","2")
console.log(sungokong.name)
console.log(sungokong.legs)
console.log(sungokong.cold_blooded)
sungokong.yell() // "Auooo"
console.log("===================================\n")
var kodok = new Frog("buduk","False","4")
console.log(kodok.name)
console.log(kodok.legs)
console.log(kodok.cold_blooded)
kodok.jump() // "hop hop" 
console.log("===================================\n")

// Soal 2 Function to Class
console.log("Soal 2 Function to Class")
class Clock{
    constructor({template}){
        this.template = template;
        this.timer=setInterval(this.render.bind(this),1000);
        this.stop = function() {
            clearInterval(this.timer);
          };
        
        this.start = function() {
            this.timer = setInterval(this.render.bind(this), 1000);
          };
    }
    render(){
      var date = new Date();
  
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;

      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    } 
}
var clock = new Clock({template: 'h:m:s'});
clock.start();