//Soal 1 Mengubah Fungsi Menjadi Fungsi Arrow ES6
console.log("Soal No.1 Mengubah Fungsi Menjadi Fungsi Arrow")
const golden = goldenFunction =>
console.log("This is golden!!")
golden()
console.log("================================\n")

//Soal 2 Sederhanakan menjadi Object Literal di ES6
console.log("Soal No.2 Sederhanakan menjadi Object Literal di ES6")
const fullName = 'William Imoh'  
const William = {
      fullName
  }
console.log(William)
console.log("================================\n")

//Soal 3 Destructuring
console.log("Soal No.3 Destructuring")
let newObject = {
    firstName : 'Harry',
    lastName : 'Potter Holt',
    destination : 'Hogwarts React Conf',
    occupation : 'Deve-Wizard Avocado',
    spell : 'Vimulus Renderus!!!'
};
const {firstName,lastName,destination,occupation,spell} = newObject;
console.log(firstName, lastName, destination, occupation)
console.log("================================\n")

//Soal 4 Array Spreading
console.log("Soal No.4 Array Spreading")
let west = ['Will', 'Chris', 'Sam', 'Holly']
let east = ['Gill', 'Brian', 'Noel', 'Maggie']

let combined = [...west, ...east]
console.log(combined)
console.log("================================\n")

//Soal 5 Template Literals
console.log("Soal No.5 Template Literals")
// const planet = "earth"
// const view = "glass"
// var before = 'Lorem ' + view + 'dolor sit amet, ' +  
//     'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
//     'incididunt ut labore et dolore magna aliqua. Ut enim' +
//     ' ad minim veniam'
 
// // Driver Code
// console.log(before) 
const planet = "earth"
const view ="glass"

const theString = `Lorem ${view} dolor sit amet, consectetur adipiscing elit,${planet} do eiusmod tempor indcididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
console.log(theString)
console.log("================================\n")