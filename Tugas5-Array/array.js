//Soal No.1 Range
console.log("Soal No.1 Range")
function range(startNum, finishNum){
    var num=[]
    if(!startNum || !finishNum){
        return -1;
    }else{
        if(startNum < finishNum){
            for(var i=startNum; i<=finishNum; i++){
                num.push(i)
            }
        }else if(startNum > finishNum){
                for(var j=startNum; j>= finishNum; j--){
                    num.push(j)
                }
        }
    }
    return num
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1
console.log("=================================================\n")

//Soal No.2 Range With Step
console.log("Soal No.2 Range With Step")
function rangeWithStep(startNum, finishNum, step){
    var num1=[]
    if(startNum > finishNum){
        for(var a=startNum; a>=finishNum; a-=step){
            num1.push(a)
        }
    }else{
        for(var b=startNum; b<=finishNum; b+=step){
            num1.push(b)
        }
    }
    return num1
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
console.log("=================================================\n")

//Soal No.3 Sum of Range
console.log("Soal No.3 Sum of Range")

function sum(angkaAwal, angkaAkhir, step) {
    var trueStep = 0
    var num3=[]

    if(!step){
        trueStep=1
    }else {
        trueStep=step
    }

    if(!angkaAwal && !angkaAkhir && !step){
        return 0
    }else if(!angkaAkhir){
        return angkaAwal
    }else {
        if(angkaAwal < angkaAkhir){
            for(var i=angkaAwal; i<=angkaAkhir; i+=trueStep){
                num3.push(i)
            }
        }else if(angkaAwal > angkaAkhir){
            for(var j=angkaAwal; j>=angkaAkhir; j-=trueStep){
                num3.push(j)
            }
        }
    }
    return num3.reduce((a,b) => a+b, 0)
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0
console.log("=================================================\n")

//Soal No.4 Array Multidimensi
console.log("Soal No.4 Array Dimensi")
function dataHandling(input) {  
    console.log("Nomor ID : " + input[0][0])
    console.log("Nama Lengkap : " + input[0][1])
    console.log("TTL : " + input[0][2] + " " + input[0][3])
    console.log("Hobi : " + input[0][4])
    console.log("\n")
    console.log("Nomor ID : " + input[1][0])
    console.log("Nama Lengkap : " + input[1][1])
    console.log("TTL : " + input[1][2] + " " + input[1][3])
    console.log("Hobi : " + input[1][4])
    console.log("\n")
    console.log("Nomor ID : " + input[2][0])
    console.log("Nama Lengkap : " + input[2][1])
    console.log("TTL : " + input[2][2]  + " " + input[2][3])
    console.log("Hobi : " + input[2][4])
    console.log("\n")
    console.log("Nomor ID : " + input[3][0])
    console.log("Nama Lengkap : " + input[3][1])
    console.log("TTL : " + input[3][2]  + " " + input[3][3])
    console.log("Hobi : " + input[3][4]) 
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
console.log(dataHandling(input))
console.log("=================================================\n")

//Soal No.5 Balik Kata
console.log("Soal No.5 Balik Kata")
function balikKata(str) {
    var currentString = str;
    var newString = "";
   for (let i = str.length - 1; i >= 0; i--) {
     newString = newString + currentString[i];
    }
    return newString;
   }
//console.log(balikKata("hello"));
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I
console.log("=================================================\n")

//Soal No.6 Metode Array
console.log("Soal No.6 Metode Array")
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]

function dataHandling2(input) {
    //mengubah nama Roman Alamsyah
    input.splice(1, 1, "Roman Alamsyah Elshawary")
    console.log(input[1])

    //mengubah Bandar Lampung
    input.splice(2,1, "Provinsi Bandar Lampung")
    console.log(input[2])

    //Menghapus "Membaca" dan menambah "Pria" dan "SMA Internasional Metro"
    input.splice(4,4, "Pria", "SMA Internasional Metro")

    // //Mengubah bulan 5 menjadi bulan mei
    var tanggalSplit = input[3].split("/")
    switch (tanggalSplit[1]) {
        case '01':
            console.log('Januari')
            break;
        case '02':
            console.log('Februari')
            break;
        case '03':
            console.log('Maret')
            break;
        case '04':
            console.log('April')
            break;
        case '05':
            console.log('Mei')
            break;
        case '06':
            console.log('Juni') 
            break;
        case '07':
            console.log('Juli')
            break;
        case '08':
            console.log('Agustus')
            break;
        case '09':
            console.log('September')
            break;
        case '10':
            console.log('Oktober')
            break;
        case '11':
            console.log('November')
            break;
        case '12':
            console.log('Desember')
            break;
        }
        tanggalSort = tanggalSplit.sort(function (a,b) { return b-a});
        console.log(tanggalSort)
        console.log(input[3].split('/').join('-'))
        console.log(input[1].slice(0,14))
}
console.log(dataHandling2(input))