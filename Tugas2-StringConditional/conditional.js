var nama ="John";
var peran = " ";

// Output untuk Input nama = '' dan peran = ''
if(nama === " " === peran === " "){
    console.log('Selamat Bergabung')
}else {
    console.log('Nama Harus Diisi!')
}
console.log()

//Output untuk Input nama = 'John' dan peran = ''
if(nama === "John" === peran === " "){
    console.log('Selamat Bermain')
}else {
    console.log('Halo John, pilih peranmu untuk memulai game!')
}
console.log()

//Output untuk Input nama = 'Jane' dan peran 'Penyihir'
if(nama === "Jane" === peran === "Penyihir"){
    console.log('Selamat datang di Dunia Werewolf, Jane')
}else {
    console.log('Halo Penyihir Jane, Kamu dapat melihat siapa yang menjadi werewolf!')
}
console.log()

//Output untuk Input nama = 'Jenita' dan peran 'Guard'
if(nama === "Jenita" === peran === "Guard"){
    console.log('Selamat datang di Dunia Werewolf, Jenita')
}else {
    console.log('Halo Guard Jane, Kamu akan membantu melindungi temanmu dari serangan werewolf.')
}
console.log()

//Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
if(nama === "Junaidi" === peran === "Werewolf"){
    console.log('Selamat datang di Dunia Werewolf, Junaidi')
}else {
    console.log('Halo Penyihir Jane, Kamu akan memakan mangsa setiap malam!')
}
console.log()
console.log("=================================================================\n");

//Soal 2 Switch Case
var hari = 21;
var bulan = 1;
var tahun = 1945;

//Maka hasil yang akan tampil di console adalah : '21 Januari 1945'
switch (hari) {
    case 1: 
        console.log("1")
        break;
    case 2: 
        console.log("2")
        break;
    case 3: 
        console.log("3")
        break;
    case 4: 
        console.log("4")
        break;
    case 5: 
        console.log("5")
        break;
    case 6: 
        console.log("6")
        break;
    case 7: 
        console.log("7")
        break;
    case 8: 
        console.log("8")
        break;
    case 9: 
        console.log("9")
        break;
    case 10: 
        console.log("10")
        break;
    case 11: 
        console.log("11")
        break;
    case 12: 
        console.log("12")
        break;
    case 13: 
        console.log("13")
        break;
    case 14: 
        console.log("14")
        break;
    case 15: 
        console.log("15")
        break;
    case 16: 
        console.log("16")
        break;
    case 17: 
        console.log("17")
        break;
    case 18: 
        console.log("19")
        break;
    case 20: 
        console.log("20")
        break;
    case 21: 
        console.log("21")
        break;
    case 22: 
        console.log("22")
        break;
    case 23: 
        console.log("23")
        break;
    case 24: 
        console.log("24")
        break;
    case 25: 
        console.log("25")
        break;
    case 26: 
        console.log("26")
        break;
    case 27: 
        console.log("27")
        break;
    case 28: 
        console.log("28")
        break;
    case 29: 
        console.log("29")
        break;
    case 30: 
        console.log("30")
        break;
    case 31: 
        console.log("31")
        break;
        default: 
        console.log("Tidak ada dalam program")
        break;
}
switch (bulan) {
    case 1: 
        console.log("Januari")
        break;
    case 2: 
        console.log("Februari")
        break;
    case 3: 
        console.log("Maret")
        break;
    case 4: 
        console.log("April")
        break;
    case 5: 
        console.log("Mei")
        break;
    case 6: 
        console.log("Juni")
        break;
    case 7: 
        console.log("Juli")
        break;
    case 8: 
        console.log("Agustus")
        break;
    case 9: 
        console.log("September")
        break;
    case 10: 
        console.log("Oktober")
        break;
    case 11: 
        console.log("November")
        break;
    case 12: 
        console.log("Desember")
        break;
    default: 
        console.log("Tidak ada dalam program")
        break;
}
switch (tahun) {
    case 1940: 
        console.log("1940")
        break;
    case 1941: 
        console.log("1941")
        break;
    case 1942: 
        console.log("1942")
        break;
    case 1943: 
        console.log("1943")
        break;
    case 1944: 
        console.log("1944")
        break;
    case 1945: 
        console.log("1945")
        break;
    case 1946: 
        console.log("1946")
        break;
    case 1947: 
        console.log("1947")
        break;
    case 1948: 
        console.log("1948")
        break;
    case 1949: 
        console.log("1949")
        break;
    case 1950: 
        console.log("1950")
        break;
    default: 
        console.log("Tidak ada dalam program")
        break;
}
console.log("=================================================================\n");

//Soal 3 mengubah dari soal 2
var hari = 11
var bulan = 7
var tahun = 1942
//Maka hasil yang akan tampil di console adalah : '11 Juli 1942'
switch (hari) {
    case 1: 
        console.log("1")
        break;
    case 2: 
        console.log("2")
        break;
    case 3: 
        console.log("3")
        break;
    case 4: 
        console.log("4")
        break;
    case 5: 
        console.log("5")
        break;
    case 6: 
        console.log("6")
        break;
    case 7: 
        console.log("7")
        break;
    case 8: 
        console.log("8")
        break;
    case 9: 
        console.log("9")
        break;
    case 10: 
        console.log("10")
        break;
    case 11: 
        console.log("11")
        break;
    case 12: 
        console.log("12")
        break;
    case 13: 
        console.log("13")
        break;
    case 14: 
        console.log("14")
        break;
    case 15: 
        console.log("15")
        break;
    case 16: 
        console.log("16")
        break;
    case 17: 
        console.log("17")
        break;
    case 18: 
        console.log("19")
        break;
    case 20: 
        console.log("20")
        break;
    case 21: 
        console.log("21")
        break;
    case 22: 
        console.log("22")
        break;
    case 23: 
        console.log("23")
        break;
    case 24: 
        console.log("24")
        break;
    case 25: 
        console.log("25")
        break;
    case 26: 
        console.log("26")
        break;
    case 27: 
        console.log("27")
        break;
    case 28: 
        console.log("28")
        break;
    case 29: 
        console.log("29")
        break;
    case 30: 
        console.log("30")
        break;
    case 31: 
        console.log("31")
        break;
        default: 
        console.log("Tidak ada dalam program")
        break;
}
switch (bulan) {
    case 1: 
        console.log("Januari")
        break;
    case 2: 
        console.log("Februari")
        break;
    case 3: 
        console.log("Maret")
        break;
    case 4: 
        console.log("April")
        break;
    case 5: 
        console.log("Mei")
        break;
    case 6: 
        console.log("Juni")
        break;
    case 7: 
        console.log("Juli")
        break;
    case 8: 
        console.log("Agustus")
        break;
    case 9: 
        console.log("September")
        break;
    case 10: 
        console.log("Oktober")
        break;
    case 11: 
        console.log("November")
        break;
    case 12: 
        console.log("Desember")
        break;
    default: 
        console.log("Tidak ada dalam program")
        break;
}
switch (tahun) {
    case 1940: 
        console.log("1940")
        break;
    case 1941: 
        console.log("1941")
        break;
    case 1942: 
        console.log("1942")
        break;
    case 1943: 
        console.log("1943")
        break;
    case 1944: 
        console.log("1944")
        break;
    case 1945: 
        console.log("1945")
        break;
    case 1946: 
        console.log("1946")
        break;
    case 1947: 
        console.log("1947")
        break;
    case 1948: 
        console.log("1948")
        break;
    case 1949: 
        console.log("1949")
        break;
    case 1950: 
        console.log("1950")
        break;
    default: 
        console.log("Tidak ada dalam program")
        break;
}
console.log("=================================================================\n");