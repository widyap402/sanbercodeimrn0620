var people = [
  ["Bruce","Banner","male",1975]];

function arrayToObject(people){
  var now = new Date();
  var count = 1;
  people.forEach(element => {
      var person = {
          firstName   : element[0],
          lastName    : element[1],
          gander      : element[2],
          age         : now.getFullYear - element[3]
      }
      console.log(count + " " + person.firstName + " " 
              + person.lastName + " : ",person);
      count++;
  });
}

arrayToObject(people);
console.log("------------------------------------------------------------\n")
var people2 = [
  ["Natasha","Romanoff","female"]];

function arrayToObject(people2){
  var now = new Date();
  var count = 1;
  people2.forEach(element => {
      var person = {
          firstName   : element[0],
          lastName    : element[1],
          gander      : element[2],
          age         : now.getFullYear - element[3]
      }
      console.log(count + " " + person.firstName + " " 
              + person.lastName + " : ",person);
      count++;
  });
}

arrayToObject(people2);
console.log("=======================================================\n")

//Soal 2
var sale = [
  ["Sepatu Stacattu", 1500000],
  ["Baju Zoro", 500000],
  ["Baju H&N", 250000],
  ["Sweater Uniklooh", 175000],
  ["Casing Handphone",50000]
];
function shoppingTIme(memberId, money){
  if(memberId == ""){
      return "Mohon maaf, toko x hanya berlaku untuk member saja";
  }
  if(money < 50000){
      return "Mohon maaf, uang tidak cukup"
  }
  var min = Math.min.apply(Math, sale.map(v => v[1]))
  var shoppingChart = {
      memberId : memberId,
      money : money,
      listPurchased : [],
      changeMoney : money
  }
  while(shoppingChart.changeMoney > min){
      sale.forEach( x => {
          if(shoppingChart.changeMoney >= x[1]){
              shoppingChart.listPurchased.push(x[0]);
              shoppingChart.changeMoney -= x[1];
              delete sale[sale.indexOf(x)];
          }
      })
      min = Math.min.apply(Math, sale.map(v => v[1]));
  }

  return shoppingChart;
}

console.log(shoppingTIme("ASDFAS",170000));
console.log("=======================================================\n")

//Soal 3
console.log("Soal No.3")
function naikAngkot(arrPenumpang){
  var rute = ['A','B','C','D','E','F'];
  var listPenumpang = [];
  arrPenumpang.forEach(x => {
      var penumpang = {
          penumpang   : x[0],
          naik        : x[1],
          turun       : x[2],
          bayar       : (rute.indexOf(x[2]) - rute.indexOf(x[1])) * 2000
      }
      listPenumpang.push(penumpang);
  })
  return listPenumpang;
}
console.log(naikAngkot([['Dimitri','B','F'],['Icha','A','B']]))